package kot

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import org.telegram.messenger.MessageObject
import org.telegram.tgnet.TLRPC.*

class KotlinFunctions() : ViewModel() {
    val repository = AdRepository()


     fun getAd() :  AdModel?  {
         /*
         * دیتا رو به صورت call response از اینترنت می گیریم.
         * برنامه به ساده ترین روش نوشته شده.
         * */
        return repository.networkService.getAd("1").execute().body()
    }



    // ساخت یک پیام خالی با عنوان Loading برای loading قبل از نمایش تبلیغ
    fun makeEmptyMessage(currentAccount :Int,lastMessage : MessageObject) : MessageObject{

        val msg: Message = TL_message()
        msg.message = "Loading ..."
        msg.action = lastMessage.messageOwner.action
        msg.id = lastMessage.messageOwner.id
        val fakeMessage = MessageObject(currentAccount, msg, true, true)
        msg.peer_id = lastMessage.messageOwner.peer_id
        msg.date = lastMessage.messageOwner.date
        msg.from_id = lastMessage.messageOwner.from_id
        fakeMessage.contentType = 10
        fakeMessage.contentType = 1
        return fakeMessage
    }


}


