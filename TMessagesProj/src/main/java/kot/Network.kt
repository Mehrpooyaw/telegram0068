package kot

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NetworkService {

    // گرفتن تبلیغ با id.
    @GET("ads/{id}")
      fun getAd(
        @Path("id") id : String
    ) : Call<AdModel>
}