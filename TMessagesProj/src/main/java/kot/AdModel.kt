package kot

data class AdModel(
    val id : String,
    val desc : String,
    val image : String,
    val title : String,
    val url : String,
)
