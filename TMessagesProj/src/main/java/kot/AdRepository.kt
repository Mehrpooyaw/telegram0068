package kot

import com.google.gson.GsonBuilder
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class AdRepository(){
    // گرفتن دیتا از اینترنت به وسیله ی کتابخانه retrofit
    val networkService : NetworkService =  Retrofit.Builder()
        .baseUrl("https://6206b3f892dd6600171c0be2.mockapi.io/api/v1/")
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()
        .create(NetworkService::class.java)
}
